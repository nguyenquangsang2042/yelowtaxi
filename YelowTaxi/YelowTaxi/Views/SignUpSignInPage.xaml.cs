﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YelowTaxi.Database;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUpSignInPage : ContentPage
    {
        DBConnection db;
        public SignUpSignInPage()
        {
            InitializeComponent();
            db = new DBConnection();
            db.createTable();
            db.insertIntoTableTypeAccout();
            
            
           
        }

        private void signupbtn_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage.Navigation.PopModalAsync();

            Navigation.PushModalAsync(new SignUpPage());
        }

        private void signinbtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new SignInPage());
        }
    }
}