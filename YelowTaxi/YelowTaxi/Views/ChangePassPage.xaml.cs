﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Toast;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YelowTaxi.Database;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangePassPage : ContentPage
    {
        string key = "";
        FirebaseHelper firebase = new FirebaseHelper();
        public ChangePassPage(string _key)
        {
            InitializeComponent();
            key = _key;
        }
        
        private async void btnChangePass_Clicked(object sender, EventArgs e)
        {
           if(entryPassOld.Text==firebase.getInfo(key).pass.ToString())
            {
                if (entryPassOld.Text == entryNewPass.Text)
                {
                    await DisplayAlert("Alert", "Your new password is same old password", "OK");
                    entryNewPass.Text = "";
                    entryPassOld.Text = "";
                    entryNewPassReplace.Text = "";
                }
                else
                {
                    if (entryNewPass.Text == entryNewPassReplace.Text && entryNewPass.Text != entryPassOld.Text)
                    {

                        firebase.updatePassword(entryNewPass.Text.ToString(), key);
                        await DisplayAlert("Alert", "You password is change", "OK");
                        entryNewPass.Text = "";
                        entryPassOld.Text = "";
                        entryNewPassReplace.Text = "";


                    }
                    else
                    {

                    }
                }
            }
           else
            {
                await DisplayAlert("Alert", "Your new password is not correct", "Try Again");
                entryNewPass.Text = "";
                entryPassOld.Text = "";
                entryNewPassReplace.Text = "";

            }
        }
    }
}