﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Manage_SettingPage : ContentPage
    {
        string key = "";
        public Manage_SettingPage(string _key)
        {
            InitializeComponent();
            key = _key;
        }

        private void imgcancel_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage.Navigation.PopModalAsync();
            Navigation.PushModalAsync(new MainPage(key));
          
        }

        private void EarnIcon_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new EarnPage());
        }

        private void WalletIcon_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new WalletPage());
        }

        private void SwitchTypeSerivebtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new SwitchServicePage(key));
        }

        private void Sumarybtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new SumaryPage());
        }

        private void Settingbtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new SettingPage(key));
        }

        private void Helpbtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new HelpPage());
        }

        private void Homebtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new PersionalInfoPage(key));
        }

        private void Subbtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new RatingPage());
        }

        private void NotifiBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new NotifiPage());
        }

        private void Exitbtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new SignUpSignInPage());
        }
    }
}