﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YelowTaxi.Database;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SwitchServicePage : ContentPage
    {
        string key = "";
        FirebaseHelper firebase = new FirebaseHelper();
        public SwitchServicePage(string _key)
        {
            InitializeComponent();
            key = _key;
            getTypeAcount(key);
            CustomeSwitch.IsEnabled = false;
            DriverSwitch.IsEnabled = false;
        }
        public void getTypeAcount(string _key)
        {
            if(firebase.getInfo(_key).typeAcccout==0)
            {
                CustomeSwitch.IsToggled = true;
                DriverSwitch.IsToggled = false;

            }    
            if(firebase.getInfo(_key).typeAcccout==1)
            {
                CustomeSwitch.IsToggled = false;
                DriverSwitch.IsToggled = true;
            }    
        }

        private void backbutton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage.Navigation.PopModalAsync();
            Navigation.PushModalAsync(new Views.Manage_SettingPage(key));
        }
    }
}