﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    
    public partial class PersionalDocumentPage : ContentPage
    {
        public PersionalDocumentPage()
        {
            InitializeComponent();

        }

        
        

        void backbutton_Clicked(System.Object sender, System.EventArgs e)
        {

        }

        void identitycardBtn_Clicked(System.Object sender, System.EventArgs e)
        {
            TakePhotoAsync();
        }
        string PhotoPath = "";
        async Task TakePhotoAsync()
        {
            
            try
            {
                var photo = await MediaPicker.CapturePhotoAsync();
                await LoadPhotoAsync(photo);
                Console.WriteLine($"CapturePhotoAsync COMPLETED: {PhotoPath}");
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Feature is now supported on the device
            }
            catch (PermissionException pEx)
            {
                // Permissions not granted
            }
            catch (Exception ex)
            {
                Console.WriteLine($"CapturePhotoAsync THREW: {ex.Message}");
            }
        }

        async Task LoadPhotoAsync(FileResult photo)
        {
            // canceled
            if (photo == null)
            {
                PhotoPath = null;
                return;
            }
            // save the file into local storage
            var newFile = Path.Combine(FileSystem.CacheDirectory, photo.FileName);
            using (var stream = await photo.OpenReadAsync())
            using (var newStream = File.OpenWrite(newFile))
                await stream.CopyToAsync(newStream);

            PhotoPath = newFile;
            returnPath(PhotoPath);
            
        }
        string getpath = "";
        string returnPath(string path)
        {
            return getpath=path;
        }

        void driverlicenseBtn_Clicked(System.Object sender, System.EventArgs e)
        {
            TakePhotoAsync();
            driverlicenseImage.Source = getpath;

        }

        void CarinsuranceBtn_Clicked(System.Object sender, System.EventArgs e)
        {
            TakePhotoAsync();
            CarinsuranceImage.Source = PhotoPath;

        }

        void profile_pictureBtn_Clicked(System.Object sender, System.EventArgs e)
        {
            TakePhotoAsync();
            profile_pictureImage.Source = PhotoPath;
        }

        void SaveBtn_Clicked(System.Object sender, System.EventArgs e)
        {
            
        }
    }
}