﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChoiseLanguagePage : ContentPage
    {
        public ChoiseLanguagePage()
        {
            InitializeComponent();
        }

        private async void nextBtn_Clicked(object sender, EventArgs e)
        {
            await Task.Delay(1000);
            Application.Current.MainPage.Navigation.PopModalAsync();
            Navigation.PushModalAsync(new SignUpSignInPage());
        }
        protected override bool OnBackButtonPressed()
        {
            System.Diagnostics.Process.GetCurrentProcess().CloseMainWindow();
            return base.OnBackButtonPressed();
        }
    }
}