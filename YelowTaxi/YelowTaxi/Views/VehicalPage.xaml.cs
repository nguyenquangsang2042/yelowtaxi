﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VehicalPage : ContentPage
    {
        public VehicalPage()
        {
            InitializeComponent();
        }

        private void AddVehicleBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new AddVehiclePage());
        }
    }
}