﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingPage : ContentPage
    {
        string key = "";
        public SettingPage(string _key)
        {
            InitializeComponent();
            key = _key;
        }

        private void MyProfileBtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new PersionalInfoPage(key));
        }

        private void MyVehiclebtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new VehicalPage());
        }

        private void PersonalDocumentbtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new PersionalDocumentPage());

        }

        private void BankDettailbtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new BankDetailsPage());

        }

        private void ChangePasswordbtn_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ChangePassPage(key));

        }

        private async void TaCbtn_Clicked(object sender, EventArgs e)
        {
            string uri = "https://www.uber.com/legal/en/document/?country=canada&lang=en&name=general-terms-of-use";
            await Browser.OpenAsync(uri, new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.AliceBlue,
                PreferredControlColor = Color.Violet
            });
        }

        private async void PrivicePoliciesBtn_Clicked(object sender, EventArgs e)
        {
            string uri = "https://help.uber.com/riders/article/privacy-notice-information?nodeId=e1f427a1-c1ab-4c6a-a78a-864f47877558";
            await Browser.OpenAsync(uri, new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.AliceBlue,
                PreferredControlColor = Color.Violet
            });
        }
        public void PlacePhoneCall(string number)
        {
            try
            {
                PhoneDialer.Open(number);
            }
            catch (ArgumentNullException anEx)
            {
                // Number was null or white space
            }
            catch (FeatureNotSupportedException ex)
            {
                // Phone Dialer is not supported on this device.
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }
        private void ContactUsbtn_Clicked(object sender, EventArgs e)
        {
            PlacePhoneCall("0123456789");
        }

        private async void AboutBtn_Clicked(object sender, EventArgs e)
        {
            string uri = "https://www.uber.com/us/en/about/";
            await Browser.OpenAsync(uri, new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.AliceBlue,
                PreferredControlColor = Color.Violet
            });
        }
    }
}