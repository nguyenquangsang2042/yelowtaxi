﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YelowTaxi.Database;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PersionalInfoPage : ContentPage
    {
        string key = "";
        FirebaseHelper firebase = new FirebaseHelper();
        public PersionalInfoPage(string _key)
        {
            InitializeComponent();
            key = _key;
            initInForUser(key);
        }
        public void initInForUser(string _key)
        {
            labelAddress.Text ="Address: "+ firebase.getInfo(_key).address;
            labelBirthDay.Text = "BirthDay: " + firebase.getInfo(_key).birthDay;
            labelNumberPhone.Text = "NumberPhone: " + firebase.getInfo(_key).numberphone;
            if(firebase.getInfo(_key).typeAcccout==0)
            {
                labelTypeAccount.Text = "Type Acount: Customer";
            }
            if (firebase.getInfo(_key).typeAcccout == 1)
            {
                labelTypeAccount.Text = "Type Acount: Driver";

            }
            labelFullName.Text = firebase.getInfo(_key).lastName + " " + firebase.getInfo(_key).firstName;





        }

        private void backbutton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage.Navigation.PopModalAsync();
            Navigation.PushModalAsync(new Views.Manage_SettingPage(key));

        }
    }
}