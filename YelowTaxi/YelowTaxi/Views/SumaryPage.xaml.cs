﻿using Microcharts;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Entry = Microcharts.ChartEntry;
namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SumaryPage : TabbedPage
    {
       
        public SumaryPage()
        {

            InitializeComponent();
            initChart();
        }

        public void initChart()
        {
            List<Entry> entries = new List<Entry> {
            new Entry(100)
            {
                Label="M",
                ValueLabel="100",
                TextColor=SKColor.Parse("#282f39"),
                ValueLabelColor=SKColor.Parse("#282f39"),
                Color = SKColor.Parse("#3DB24B")
            },
            new Entry(400)
            {
                Label="T", ValueLabel="100",
                Color = SKColor.Parse("#3DB24B"),
                TextColor=SKColor.Parse("#282f39"),
                ValueLabelColor=SKColor.Parse("#282f39")
            },
            new Entry(12)
            {
                Label="Wed", ValueLabel="100",
                Color = SKColor.Parse("#3DB24B"),
                TextColor=SKColor.Parse("#282f39"),
                ValueLabelColor=SKColor.Parse("#282f39")
            },
            new Entry(224)
            {
                Label="Mon", ValueLabel="100",
                Color = SKColor.Parse("#3DB24B"),
                TextColor=SKColor.Parse("#282f39"),
                ValueLabelColor=SKColor.Parse("#282f39")
            },
            new Entry(10)
            {
                Label="Mon", ValueLabel="100",
                 Color = SKColor.Parse("#3DB24B"),
                TextColor=SKColor.Parse("#282f39"),
                ValueLabelColor=SKColor.Parse("#282f39")

            },
            new Entry(50)
            {
                Label="Mon",
                 ValueLabel="100",
                Color = SKColor.Parse("#3DB24B"),
                TextColor=SKColor.Parse("#282f39"),
                ValueLabelColor=SKColor.Parse("#282f39")
            },
        };
            var chart = new BarChart()
            {
                Entries = entries,
                LabelTextSize = 19

            };
            this.ChartViewSumary.Chart = chart;
        }
    }
}