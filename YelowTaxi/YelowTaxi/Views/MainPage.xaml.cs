﻿using MapApp;
using Newtonsoft.Json;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace YelowTaxi
{
    public partial class MainPage : ContentPage
    {
        string key = "";
        public MainPage(string _key)
        {
            InitializeComponent();
            Task.Delay(2000);
            checkPosition();
            key = _key;
        }

         async void checkPosition()
        {
            try
            {
                var location = await Geolocation.GetLastKnownLocationAsync();
                Pin mypin = new Pin()
                {
                    Type = PinType.Place,
                    Label = "My Location",
                    Position = new Position(location.Latitude, location.Longitude),

                };
                MyMap.Pins.Add(mypin);
                MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(mypin.Position, Distance.FromMeters(5000)));
                
                if (location != null)
                {
                    entryYourLoaction.Text="Latitude:"+ location.Latitude+","+ "Longitude:" +location.Longitude+" Altitude:" +location.Altitude;
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
            }
            catch (Exception ex)
            {
                // Unable to get location
            }
        }

        //List<Place> placesList = new List<Place>();
       
        //private async void UpdateMap()
        //{
        //    try
        //    {
        //        var assembly = IntrospectionExtensions.GetTypeInfo(typeof(MainPage)).Assembly;
        //        Stream stream = assembly.GetManifestResourceStream("MapApp.Places.json");
        //        string text = string.Empty;
        //        using (var reader = new StreamReader(stream))
        //        {
        //            text = reader.ReadToEnd();
        //        }

        //        var resultObject = JsonConvert.DeserializeObject<Places>(text);

        //        foreach (var place in resultObject.results)
        //        {
        //            placesList.Add(new Place
        //            {
        //                PlaceName = place.name,
        //                Address = place.vicinity,
        //                Location = place.geometry.location,
        //                Position = new Position(place.geometry.location.lat, place.geometry.location.lng),
        //                //Icon = place.icon,
        //                //Distance = $"{GetDistance(lat1, lon1, place.geometry.location.lat, place.geometry.location.lng, DistanceUnit.Kiliometers).ToString("N2")}km",
        //                //OpenNow = GetOpenHours(place?.opening_hours?.open_now)
        //            });
        //        }

        //        MyMap.ItemsSource = placesList;
        //        //PlacesListView.ItemsSource = placesList;
        //        //var loc = await Xamarin.Essentials.Geolocation.GetLocationAsync();
        //        MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(47.6370891183, -122.123736172), Distance.FromKilometers(100)));

        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex);
        //    }


        //}

        private void avatar_imgButtonmini_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new Views.Manage_SettingPage(key));
        }
    }
}
