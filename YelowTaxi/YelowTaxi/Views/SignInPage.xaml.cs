﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Toast;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YelowTaxi.Database;
namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignInPage : ContentPage
    {
        DBConnection db;
        FirebaseHelper firebase = new FirebaseHelper();
        public SignInPage()
        {
            InitializeComponent();
            db = new DBConnection();
        }

       

        private void signinbtn_Clicked(object sender, EventArgs e)
        {
            string numberphone = entryNBphone.Text.ToString();
            string pass = entryPass.Text.ToString();
            string typereturn=firebase.checkLogin(numberphone, pass);
            
            if(typereturn=="True")
            {
                Navigation.PushModalAsync(new MainPage(firebase.getkey(entryNBphone.Text,entryPass.Text)));
            }
            else
            {
               
            }
            
           /* if (!db.checkDB(entryNBphone.Text))
            {
                Navigation.PushModalAsync(new MainPage());
                CrossToastPopUp.Current.ShowToastMessage("login success");
            }
            else
            {
                if(entryNBphone==null)
                {
                    CrossToastPopUp.Current.ShowToastMessage("type numberphone and pass to login");
                }    
                else
                {
                    CrossToastPopUp.Current.ShowToastMessage("account isn't already");
                }    
            }   */
          
        }

        private void backbutton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage.Navigation.PopModalAsync();

            Navigation.PushModalAsync(new SignUpSignInPage());
        }
    }
}