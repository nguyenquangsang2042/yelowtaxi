﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entry = Microcharts.ChartEntry;
using Microcharts;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SkiaSharp;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RatingPage : ContentPage
    {
        public RatingPage()
        {
            InitializeComponent();
            Sumary_Rating();
        }
        void Sumary_Rating()
        {
            List<Entry> entries = new List<Entry>
            {
                new Entry(100)
                {
                    Label = "M",
                    ValueLabel = "100",
                    TextColor = SKColor.Parse("#282f39"),
                    ValueLabelColor = SKColor.Parse("#282f39"),
                    Color = SKColor.Parse("#3DB24B")
                },
                
            };
            var chart = new RadialGaugeChart()
            {
                Entries = entries,
                LabelTextSize = 19,
                MaxValue=500

            };
            this.RatingChart.Chart = chart;
        }
    }
}