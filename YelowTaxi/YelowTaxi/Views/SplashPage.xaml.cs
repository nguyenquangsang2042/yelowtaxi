﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class SplashPage : ContentPage
    {
        public SplashPage()
        {
            InitializeComponent();
            delaySpashlayout();
        }
        public async void delaySpashlayout()
        {
            await Task.Delay(2000);
            Navigation.PushModalAsync(new ChoiseLanguagePage());
        }
    }
}