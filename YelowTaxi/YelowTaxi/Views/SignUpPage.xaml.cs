﻿using Plugin.Toast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YelowTaxi.Database;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUpPage : ContentPage
    {
        public SignUpPage()
        {
            InitializeComponent();
            pickerNBphone.Items.Add("+84");
            pickerNBphone.SelectedIndex = 0;
            btnCotinueSignUp.IsEnabled = false;


        }
        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
        }

        private void btnCotinueSignUp_Clicked(object sender, EventArgs e)
        {
           if(entryNbphone.Text==null||entryNbphone.Text=="")
            {
                CrossToastPopUp.Current.ShowToastMessage("Type number phone in entry to continue");
            }
           else
            {
                Navigation.PushModalAsync(new CreateProfilePage(pickerNBphone.SelectedIndex,entryNbphone.Text));
            }
        }

        private void CheckboxTerm_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if(CheckboxTerm.IsChecked)
            {
                btnCotinueSignUp.IsEnabled = true;
                btnCotinueSignUp.BackgroundColor = Color.FromHex("#3db24b");
            }
            else
            {
                btnCotinueSignUp.IsEnabled = false;
                btnCotinueSignUp.BackgroundColor = Color.Gray;
            }
        }

        private void backbutton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage.Navigation.PopModalAsync();

            Navigation.PushModalAsync(new SignUpSignInPage());
            
        }
    }
}