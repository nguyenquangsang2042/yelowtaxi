﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YelowTaxi.Database;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Toast;
using System.Collections.Generic;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;

namespace YelowTaxi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateProfilePage : ContentPage
    {
        FirebaseHelper firebaseHelper = new FirebaseHelper();
        DBConnection db = new DBConnection();
        List<TypeAccount> listType = new List<TypeAccount>();
        string typeAccout = "";
        UserManagement userManagement = new UserManagement();
       
        public CreateProfilePage(int firstnbPhone,string nbphone)
        {
            InitializeComponent();
           
            listType = db.selectALlTypeAccout();
            
            foreach(TypeAccount typeAccount in listType)
            {
                pickerTypeAccout.Items.Add(typeAccount.type);
            }
            pickerTypeAccout.SelectedIndex = 0;
            pickerNumberPhone.Items.Add("+84");
            pickerNumberPhone.SelectedIndex = 0;
            btnRegister.BackgroundColor = Color.Gray;
            btnRegister.IsEnabled = false;
            entryNBphone.Text = nbphone;
            pickerNumberPhone.SelectedIndex = firstnbPhone;
           

        }

        private void checkBox_createProfile_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if(checkBox_createProfile.IsChecked)
            {
                btnRegister.IsEnabled = true;
                btnRegister.BackgroundColor = Color.FromHex("#3db24b");
                
               
            }
            else
            {
                btnRegister.IsEnabled = false;
                btnRegister.BackgroundColor = Color.Gray;

            }
        }

        private async void btnRegister_Clicked(object sender, EventArgs e)
        {

            userManagement = new UserManagement
            {
                firstName = entryFirstName.Text,
                lastName = entryLastName.Text,
                birthDay = entrybirthday.Date.ToString("dd/MM/yyyy"),
                typeAcccout = pickerTypeAccout.SelectedIndex,
                numberphone = "0" + entryNBphone.Text,
                address = entryAddress.Text,
                pass = entryPass.Text
            };
            firebaseHelper.addUser(userManagement);

        }

        private void pickerNumberPhone_SelectedIndexChanged(object sender, EventArgs e)
        {
            //djskajd
        }

        private void entryFirstName_Completed(object sender, EventArgs e)
        {
            entryLastName.Focus();
        }

        private void entryLastName_Completed(object sender, EventArgs e)
        {
            entryAddress.Focus();
        }

        private void entryAddress_Completed(object sender, EventArgs e)
        {
            entryPass.Focus();
        }

        private void entryPass_Completed(object sender, EventArgs e)
        {
            entrybirthday.Focus();
        }

        private void pickerTypeAccout_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pickerTypeAccout.SelectedIndex == 0)
            {
                typeAccout = "0";
            }
            else
            {
                typeAccout = "1";
            }

        }

        private void entrybirthday_DateSelected(object sender, DateChangedEventArgs e)
        {
            pickerTypeAccout.Focus();
        }
    }
}