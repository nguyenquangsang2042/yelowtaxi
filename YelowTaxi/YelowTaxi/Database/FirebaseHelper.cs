﻿using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json;
using Plugin.Toast;
using System.Threading.Tasks;

namespace YelowTaxi.Database
{
    class FirebaseHelper
    {
        IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = "KOZLUYFcNr11TNzKPOvIwi8kgtCmmU9GA2XHM5RT",
            BasePath = "https://yelowtaxiproject-bd0a3-default-rtdb.firebaseio.com/"
        };
        IFirebaseClient client;
       

        public async void addUser(UserManagement user)
        {
            client = new FireSharp.FirebaseClient(config);
           
            PushResponse response = await client.PushAsync("UserManagerment", user);
        }
        public  string checkLogin(string numberphone,string pass)
        {
            client = new FireSharp.FirebaseClient(config);
            FirebaseResponse response =  client.Get("UserManagerment");
            Dictionary<String, UserManagement> data = JsonConvert.DeserializeObject<Dictionary<String, UserManagement>>(response.Body.ToString());
            bool type=false;
            string key = "";
           foreach(var item in data)
            {
                if(numberphone==item.Value.numberphone && pass == item.Value.pass)
                {
                    type= true;
                    key = item.Key.ToString();
                    break;
                }
                else if(numberphone!=item.Value.numberphone && pass!=item.Value.pass)
                {
                    type= false;
                }
            }
            Console.WriteLine(key.ToString());
            return type.ToString();


            //The response will contain the data being retreived
        }
        public UserManagement getInfo(string key)
        {
            client = new FireSharp.FirebaseClient(config);
            FirebaseResponse response = client.Get("UserManagerment");
            Dictionary<String, UserManagement> data = JsonConvert.DeserializeObject<Dictionary<String, UserManagement>>(response.Body.ToString());
            UserManagement getInfo = new UserManagement() ;
            foreach (var item in data)
            {
                if (key == item.Key)
                {
                    getInfo.firstName = item.Value.firstName;
                    getInfo.lastName = item.Value.lastName;
                    getInfo.typeAcccout = item.Value.typeAcccout;
                    getInfo.numberphone = item.Value.numberphone;
                    getInfo.pass = item.Value.pass;
                    getInfo.birthDay = item.Value.birthDay;
                    getInfo.address = item.Value.address;
                    break;
                }
                else
                {
                    getInfo = null;
                }
            }
            return getInfo;
        }
        public async void updatePassword(string newpass,string key)
        {
            UserManagement user = getInfo(key);
            user.pass = newpass;

            FirebaseResponse response = await client.UpdateAsync("UserManagerment/"+key,user);
        }
        public string getkey(string numberphone,string pass)
        {

            client = new FireSharp.FirebaseClient(config);
            FirebaseResponse response = client.Get("UserManagerment");
            Dictionary<String, UserManagement> data = JsonConvert.DeserializeObject<Dictionary<String, UserManagement>>(response.Body.ToString());
            string key = "";
            foreach (var item in data)
            {
                if (numberphone == item.Value.numberphone)
                {
                    key = item.Key.ToString();
                    break;
                }
                else if (numberphone != item.Value.numberphone && pass != item.Value.pass)
                {
                    key = "error";
                }
            }
            return key.ToString();

        }
        public async void addVehicle()
        {
            client = new FireSharp.FirebaseClient(config);
            var _userManagement = new UserManagement
            {

            };
            PushResponse response = await client.PushAsync("UserManagerment", _userManagement);
        }
    }
}
