﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
namespace YelowTaxi.Database
{
    class Rating
    {
        [NotNull]
        public string idUser { get;set;}
        [NotNull]
        public string idCustomer { get;set;}
        public string comment { get;set;}
        [NotNull]
        public int nvrating { get; set; }
    }
}
