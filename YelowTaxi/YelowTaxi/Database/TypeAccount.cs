﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace YelowTaxi.Database
{
    class TypeAccount
    {
        [PrimaryKey,NotNull]
        public int code { get; set; }
        [NotNull]
        public string type { get; set; }
    }
}
