﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace YelowTaxi.Database
{
    class Pay
    {
        [PrimaryKey]
        public string id { get; set; }
        [NotNull]
        public string idUser { get; set; }
        [NotNull]
        public string date { get; set; }
        [NotNull]
        public string pay { get; set; }
        [NotNull]
        public string codeTrip { get; set; }
    }
}
