﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
namespace YelowTaxi.Database
{
    class Trip
    {
        [PrimaryKey]
        public string id { get;set;}
        [NotNull]
        public string idDriver { get;set;}
        [NotNull]
        public string idCustomer { get;set; }
        [NotNull]
        public string idVehicle { get;set; }
        [NotNull]
        public string fee { get; set; }
        [NotNull]
        public string _time { get;set;}
        [NotNull]
        public string kilometer { get;set;}
    }
}
