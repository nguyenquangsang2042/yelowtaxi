﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace YelowTaxi.Database
{
    class PersonalDocument_Driver
    {
        [PrimaryKey]
        public string idUser { get;set;}
        [NotNull]
        public string bithCer { get;set;}
        [NotNull]
        public string divingLicence { get;set;}
        [NotNull]
        public string ideniliti_card { get;set;}
    }
}
