﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace YelowTaxi.Database
{
    class Vehicle
    {
        [PrimaryKey]
        public string id { get;set;}
        [NotNull]
        public string idUser { get;set;}
        [NotNull]
        public string license_plates { get;set;}
        [NotNull]
        public string car_manufacturer { get;set;}
        [NotNull]
        public string car_model { get;set;}
    }
}
