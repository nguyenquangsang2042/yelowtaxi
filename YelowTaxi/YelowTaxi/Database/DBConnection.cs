﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace YelowTaxi.Database
{
    class DBConnection
    {
        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        public bool createTable()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Yelow.db")))
                {
                    connection.CreateTable<Earning>();
                    connection.CreateTable<Pay>();
                    connection.CreateTable<PersonalDocument_Driver>();
                    connection.CreateTable<Rating>();
                    connection.CreateTable<Trip>();
                    connection.CreateTable<TypeAccount>();
                    connection.CreateTable<UserManagement>();
                    connection.CreateTable<Vehicle>();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool insertIntoTableTypeAccout()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Yelow.db")))
                {
                    connection.Insert(new TypeAccount { code=0,type="Customer"}); 
                    connection.Insert(new TypeAccount { code=1,type="Driver"});
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                return false;
            }
        }
        public bool insertIntoTableUserManagement(UserManagement userManagement)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Yelow.db")))
                {
                    connection.Insert(userManagement);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                return false;
            }
        }
        public bool checkDB(string numberphone)
        {
            var database = new SQLiteConnection(System.IO.Path.Combine(folder, "Yelow.db"));
            var data = database.Table<UserManagement>();
            var datacheck = data.Where(x => x.numberphone.Equals(numberphone)).FirstOrDefault();
            if (datacheck != null)
                return false;
            else
                return true;
        }
            public List<TypeAccount> selectALlTypeAccout()
        {

            var conn = new SQLiteConnection(System.IO.Path.Combine(folder, "Yelow.db"));
            string query = "SELECT * FROM TypeAccount";
            var words = conn.Query<TypeAccount>(query);
            return words;
        }
    }
}
