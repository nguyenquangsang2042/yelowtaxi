﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace YelowTaxi.Database
{
    class UserManagement
    {
       [PrimaryKey]
        public string id { get; set; }
        [NotNull]
        public string firstName { get; set; }
        [NotNull]
        public string lastName { get; set; }
        [NotNull]
        public string birthDay { get; set; }
        [NotNull]
        public int typeAcccout { get; set; }
        [NotNull]
        public string numberphone { get; set; }
        [NotNull]
        public string address { get; set; }
        [NotNull]
        public string pass { get; set; }

    }
}
