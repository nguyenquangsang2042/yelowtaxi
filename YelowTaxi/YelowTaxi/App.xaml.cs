﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using YelowTaxi.Views;

// add font
[assembly: ExportFont("NunitoSansBold.ttf", Alias = "NunitoSansBold")]
[assembly: ExportFont("NunitoSansRegular.ttf", Alias = "NunitoSansRegular")]
namespace YelowTaxi
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new SignUpSignInPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
